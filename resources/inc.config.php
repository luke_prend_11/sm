<?php
// Keep error reporting turned on during testing
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);

// Create autoload function for classes
function __autoload($class_name)  
{  
	include_once dirname(__FILE__).'/../resources/library/class.'.$class_name.'.php';  
}

// Instantiate class libraries to be used
new config();
$db = new database();
$mc = new meta_content();
new notifications();
?>