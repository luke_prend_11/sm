<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title><?php echo $mc->pageTitle; ?></title>
<meta name="Description" content="<?php echo $mc->metaDescription; ?>" />
<meta name="Keywords" content="<?php echo $mc->metaKeywords; ?>" />
<meta name="Developer" content="Luke Prendergast" />

<link rel="stylesheet/less" type="text/css" href="<?php echo config::$baseUrl; ?>/css/reset.less" />
<link rel="stylesheet/less" type="text/css" href="<?php echo config::$baseUrl; ?>/css/master.less" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<?php echo $mc->css; ?>

<link href='https://fonts.googleapis.com/css?family=Open+Sans:800,400,300' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="<?php echo config::$baseUrl; ?>/img/favicon.ico" type="image/x-icon" />
<link rel="icon" href="<?php echo config::$baseUrl; ?>/img/favicon.ico" type="image/x-icon">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="<?php echo config::$baseUrl; ?>/js/less.min.js" type="text/javascript"></script>
<script src="<?php echo config::$baseUrl; ?>/js/parallax.js"></script>
<script src="<?php echo config::$baseUrl; ?>/js/responsive-nav.js"></script>

<!-- Force html5 elements to work in ie7 and ie8 -->
<!--[if lte IE 8]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!-- Force media queries to work in ie7 and ie8 -->
<script type="text/javascript" src="<?php echo config::$baseUrl; ?>/js/respond.js"></script>

<?php
if(!isset($_COOKIE['eucookie'])) {
?>
	<!-- If EU Cookie Law hasn't been accepted then run script -->
	<script type="text/javascript">
    function SetCookie(c_name,value,expiredays)
    {
    var exdate=new Date()
    exdate.setDate(exdate.getDate()+expiredays)
    document.cookie=c_name+ "=" +escape(value)+";path=/"+((expiredays==null) ? "" : ";expires="+exdate.toGMTString())
    }
    </script>
<?php
}
?>

<?php
if($mc->lightBox == TRUE) {
?>
<script type="text/javascript" src="<?php echo config::$baseUrl; ?>/js/jquery.lightbox-0.5.js"></script>
<script type="text/javascript">
    $(function() {
        $('#gallery a').lightBox();
    });
</script>
<?php
}
?>

<?php
if(!strpos(config::BASE_URL_TEST, $_SERVER['SERVER_NAME']) && !strpos(config::BASE_URL_LIVE_TESTING, $_SERVER['SERVER_NAME']))
{
?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-75388300-1', 'auto');
  ga('send', 'pageview');

</script>

<?php
}
?>
</head>
<body>
<?php
if(!isset($_COOKIE['eucookie'])) {
?>
	<!-- If EU Cookie Law hasn't been accepted then display message and run script -->
	<div id="eucookielaw" >
		<div class="content">
			<p>This website uses cookies to track visitor activity and behaviour. By browsing our site you agree to our use of cookies.</p>
			<a id="removecookie" title="Accept Cookies" class="btn dark-grey float-left margin-right">Accept</a>
			<a id="more" href="<?php echo config::$baseUrl; ?>/help/privacy.php" title="Privacy Policy" class="btn dark-grey float-left">Find Out More</a>
			<br class="clearfloat" />
		</div>
	</div>
	<script type="text/javascript">
    if( document.cookie.indexOf("eucookie") ===-1 ){
    $("#eucookielaw").show();
    }
    $("#removecookie").click(function () {
    SetCookie('eucookie','eucookie',365*10)
    $("#eucookielaw").remove();
    });
    </script>
<?php
}
?>
	<header>
		<div id="menu-bar">
			<div class="content">
				<a href="<?php echo config::$baseUrl; ?>" title="<?php echo config::SITE_NAME; ?> Home">
					<img src="<?php echo config::$baseUrl; ?>/img/layout/logo.png" alt="<?php echo config::SITE_NAME; ?> Logo" class="logo" />
				</a>

				<nav class="nav-collapse">
					<ul>
						<li><a href="<?php echo config::$baseUrl; ?>" title="<?php echo config::SITE_NAME; ?>">Home</a></li>
						<li><a href="<?php echo config::$baseUrl; ?>/services.php" title="Services Provided">Our Services</a></li>
						<li><a href="<?php echo config::$baseUrl; ?>/gallery.php" title="Recent Projects">Our Work</a></li>
						<li><a href="<?php echo config::$baseUrl; ?>/help/about-us.php" title="About Us">About Us</a></li>
						<li class="alignRight"><a href="<?php echo config::$baseUrl; ?>/help/contact-us.php" title="Contact Us" class="no-border">Contact Us</a></li>
					</ul>
				</nav>
			</div>

			<br class="clearfloat" />
		</div>
		<div id="contact-bar" class="very-light-grey">
			<div class="content">
				<span class="phone">
					<strong>
						<a href="tel:<?php echo config::CONTACT_PHONE; ?>" title="Call <?php echo config::SITE_NAME; ?>">
							<?php echo config::CONTACT_PHONE; ?>
						</a>
					</strong> |
					<strong>
						<a href="tel:<?php echo config::CONTACT_PHONE_ALT; ?>" title="Call <?php echo config::SITE_NAME; ?>">
							<?php echo config::CONTACT_PHONE_ALT; ?>
						</a>
					</strong>
				</span> 
				<span class="email">
					<strong>
						<a href="mailto:<?php echo config::CONTACT_EMAIL; ?>" title="Email <?php echo config::SITE_NAME; ?>">
							<?php echo config::CONTACT_EMAIL; ?>
						</a>
					</strong>
				</span>
			</div>
		</div>
		<div id="header-image"></div>
		<div class="parallax-window" data-parallax="scroll" data-image-src="<?php echo config::$baseUrl; ?>/img/layout/banner-image.jpg" data-natural-width="1500" data-natural-height="500"></div>
	</header>

	<div class="content">
		<?php
		$uu = new url_utils();
		// show breadcrumb and h1 if current page is not the home page
		if($uu->curPageURL() != config::$baseUrl.'/') {
			$bc = new breadcrumb();
			echo '<span class="breadcrumb">'.$bc->showBreadcrumb().'</span>';
			echo '<h1>'.$mc->pageH1.'</h1>';
		}
		?>
