		<br class="clearfloat" />
	</div>

	<footer>
		<div id="links">
			<div class="content">
				<ul class="float-left">
					<li>
						<a href="<?php echo config::$baseUrl; ?>" title="Home Page">Home</a>
					</li>
					<li>
						<a href="<?php echo config::$baseUrl; ?>/help/about-us.php" title="About Us">About Us</a>
					</li>
					<li>
						<a href="<?php echo config::$baseUrl; ?>/help/contact-us.php" title="Contact Us">Contact Us</a>
					</li>
					<li>
						<a href="<?php echo config::$baseUrl; ?>/gallery.php" title="Recent Projects">Our Work</a>
					</li>
					<li>
						<a href="<?php echo config::$baseUrl; ?>/services.php" title="Our Services">Our Services</a>
					</li>
					<li>
						<a href="<?php echo config::$baseUrl; ?>/help/privacy.php" title="Privacy Policy">Privacy Policy</a>
					</li>
					<li>
						<a href="<?php echo config::$baseUrl; ?>/site-map.php" title="Site Map">Site Map</a>
					</li>
				</ul>
				
				<div class="float-right">
					<h4>Contact Info</h4>
					<span class="phone">
						<a href="tel:<?php echo config::CONTACT_PHONE; ?>" title="Call <?php echo config::SITE_NAME; ?>">
							<?php echo config::CONTACT_PHONE; ?>
						</a>
					</span>
					<span class="email">
						<a href="mailto:<?php echo config::CONTACT_EMAIL; ?>" title="Email <?php echo config::SITE_NAME; ?>">
							<?php echo config::CONTACT_EMAIL; ?>
						</a>
					</span>
				</div>
				<br class="clearfloat" />
			</div>
		</div>

		<div id="copyright">
			<div class="content">
				<span>
					<?php echo config::DOMAIN; ?> &copy; <?php echo date('Y'); ?>. All Rights Reserved | 
                    Website Designed &amp; Developed by <a href="https://lpwebdesign.co.uk" title="LP Web Design" target="_blank">LP Web Design</a>
				</span>
			</div>
		</div>
	</footer>
	
	<!-- smooth scroll anchor links -->
	<script type="text/javascript">
		var jsvar = window.location.hash;
		
		if (jsvar) {
			$(document).ready(function(){
				event.preventDefault();
				$('html, body').animate({
					scrollTop: $(jsvar).offset().top
				}, 1000);
				return false;
			});
		}
	</script>
	
	<!-- responsive navigation menu -->
	<script>
	  	var nav = responsiveNav(".nav-collapse");
	</script>
</body>
</html>
