<?php
class database
{	
	public function bindParams(&$a_bind_params, $a_param_type)
	{
		// Bind parameters. Types: s = string, i = integer, d = double,  b = blob
		$a_params = array();
		 
		$param_type = '';
		$n = count($a_param_type);
		for($i = 0; $i < $n; $i++) {
		    $param_type .= $a_param_type[$i];
		}
		 
		// with call_user_func_array, array params must be passed by reference
		$a_params[] = &$param_type;
		 
		for($i = 0; $i < $n; $i++) {
			// with call_user_func_array, array params must be passed by reference
			$a_params[] = &$a_bind_params[$i];
		}
		return $a_params;
	}
	
	public function prepInsert($table, $fields, $a_bind_params, $a_param_type)
	{
		// join fields in the format field1, field2
		$comma_sep_fields = implode(", ", $fields);
		$comma_sep_qm = '?';
		
		// add , ? for every additional field to be inserted
		if(is_array($fields)) {
			$counter = count($fields);
			for($i = 2; $i <= $counter; $i++) {
				$comma_sep_qm .= ', ?';
			}
		}
		
		// prepare the insert statement
		$stmt = config::$mysqli->prepare("INSERT INTO ".$table." (".$comma_sep_fields.") VALUES (".$comma_sep_qm.")");
 		
		// send parmaeters and parameter types to bind_param function
		$a_params = $this->bindParams($a_bind_params, $a_param_type);
				
		// use call_user_func_array, as $stmt->bind_param('s', $param); does not accept params array
		call_user_func_array(array($stmt, 'bind_param'), $a_params);
		
		return $stmt;
	}
	
	// used this function for SELECT and UPDATE statements
	public function prepQuery($query, $a_bind_params, $a_param_type)
	{
		// prepare the select statement
		$stmt = config::$mysqli->prepare($query);
		
		// send parmaeters and parameter types to bind_param function
		$a_params = $this->bindParams($a_bind_params, $a_param_type);
		
		// use call_user_func_array, as $stmt->bind_param('s', $param); does not accept params array
		call_user_func_array(array($stmt, 'bind_param'), $a_params);
		
		return $stmt;
	}
	
	public function executeStmt($stmt)
	{
		// execute statement
		if($stmt->execute()) {
			$stmt->store_result();
			return $stmt;
		}
		else {
			$stmt->close();
			return false;
		}
	}
	
	public function completeQuery($query, $a_bind_params, $a_param_type)
	{
		$stmt = $this->prepQuery($query, $a_bind_params, $a_param_type);
		if($this->executeStmt($stmt)) {
			return $stmt;
		}
		return false;
	}
	
	public function completeInsert($table, $fields, $a_bind_params, $a_param_type)
	{
		$stmt = $this->prepInsert($table, $fields, $a_bind_params, $a_param_type);
		if($this->executeStmt($stmt)) {
			$id = config::$mysqli->insert_id;
			$this->insertUpdateTime($table);
			echo $id;
			return true;
		}
		return false;
	}
	
	public function insertUpdateTime($table_name)
	{
		// check to see if table has already been updated
		$tblCheck = $this->completeQuery('SELECT table_name FROM table_updates WHERE table_name = ? LIMIT 1', array($table_name), array('s'));
		if($tblCheck->num_rows != 0) {
			// table already entered so perform update
			
			// first select last update time from table you are updating
			$lstUpdate = $this->completeQuery('SELECT last_update FROM table_updates WHERE table_name = ? LIMIT 1', array($table_name), array('s'));
			$lstUpdate->bind_result($last_update);
			$lstUpdate->fetch();
			$lstUpdate->close();
			
			// then perform the update with last_update and previous_update
			$stmt = $this->prepQuery('UPDATE table_updates SET last_update = ?, previous_update = ? WHERE table_name = ? LIMIT 1', array(config::$curDate, $last_update, $table_name), array('s','s','s'));
		}
		else {
			// table not entered so perform insert
			$stmt = $this->prepInsert('table_updates', array('table_name', 'last_update'), array($table_name, config::$curDate), array('s', 's'));
		}
		$tblCheck->close();
		
		// execute insert or update function
		if($this->executeStmt($stmt)) {
			$stmt->close();
			return true;
		}
		return false;	
	}
	
	public function showUpdateTime($table_name)
	{
		$stmt = $this->completeQuery(
		'SELECT last_update 
		FROM table_updates
		WHERE table_name = ?
		LIMIT 1',
		array($table_name),
		array('s'));
		if ($stmt->num_rows != 0) {
			$stmt->bind_result($last_update);
			$stmt->fetch();
			$stmt->close();
			
			return '<span class="last-update">Page last updated on '.date("l d F Y", strtotime($last_update)).' at '.date("g:ia", strtotime($last_update)).'.</span>';
		}
		else {
			$stmt->close();
			return false;
		}
	}
}
?>