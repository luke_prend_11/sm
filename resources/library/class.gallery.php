<?php
class gallery
{	
    const DIRECTORY = 'img/content/gallery/%s/';

    private $categories = array('Domestic', 'Commercial', 'Industrial', 'Education');

    private $currentCategory;
    private $currentCategoryDirectory;
    private $currentCategoryFiles;

    public function getPhotos()
    {
        foreach($this->categories as $cat)
        {
            $this->setCurrentCategory($cat);
            $this->setCurrentCategoryDirectory();
            $this->setCurrentCategoryFiles();
            $this->buildCategoryGallery();
        }
    }
	
    private function setCurrentCategory($cat)
    {
        $this->currentCategory = $cat;
    }

    private function setCurrentCategoryDirectory()
    {
        $this->currentCategoryDirectory = sprintf(self::DIRECTORY, strtolower($this->currentCategory));
    }

    private function setCurrentCategoryFiles()
    {
        $this->currentCategoryFiles = $this->getFiles();
    }

    private function getFiles()
    {
        return glob($this->currentCategoryDirectory."*.{jpg,png,JPG,GIF}", GLOB_BRACE);
    }
	
    private function buildCategoryGallery()
    {
        echo '
        <h2 id="'.strtolower($this->currentCategory).'">'.$this->currentCategory.'</h2>';

        if(!empty($this->currentCategoryFiles))
        {
            $this->getGallery();
            
            $this->currentCategory == "Industrial" ? $this->getRainhamSteelPhotos() : '';
        }
        else {
            echo 'There are currently no photos available for this category.';
        }
    }
    
    private function getGallery()
    {
        echo '
        <div id="gallery">
        <ul>';
        
        foreach($this->currentCategoryFiles as $file)
        {
            $this->getGalleryItem($file);
        }

        echo '
        </ul>
        <br class="clearfloat" />
        </div>';
    }
    
    private function getGalleryItem($file)
    {
        $filePath = config::$baseUrl.'/'.$file;
        
        echo '
        <li>
            <a href="'.$filePath.'">
                <div class="sq-thumb" style="background-image:url(\''.$filePath.'\');"></div>
            </a>
        </li>';
    }
    
    private function getRainhamSteelPhotos()
    {
        $this->setCurrentCategory('RainhamSteel');
        $this->setCurrentCategoryDirectory();
        $this->setCurrentCategoryFiles();
        
        echo '<h3>Rainham Steel</h3>';

        $this->getGallery();
        
        $this->getRainhamSteelEssexPhotos();
    }
    
    private function getRainhamSteelEssexPhotos()
    {
        $this->setCurrentCategory('RainhamSteelEssex');
        $this->setCurrentCategoryDirectory();
        $this->setCurrentCategoryFiles();
        
        echo '<h3>Rainham Steel - Essex</h3>';

        $this->getGallery();
    }
}
