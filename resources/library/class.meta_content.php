<?php
class meta_content
{	
	const DEFAULT_META_KEYWORDS    = "painting, decorators, contractors, paint and decorating, painting and decorating contractors, commercial painting, indeustrial painting, domestic painting and decorating";
	const DEFAULT_META_DESCRIPTION = "";
	const STYLESHEET_LINK          = '<link rel="stylesheet/less" type="text/css" href="%s/css/%s" />';
	
	public $currentPageRoute;
	
	public $pageTitle;
	public $metaDescription;
	public $metaKeywords;
	public $pageH1;
	public $pageH2;
	public $css;
	public $lightBox;
	
	private $metaContent = array();
	
	public function __construct()
	{
		$this->getCurrentPage();
		
		$this->setMetaContent();
		$this->setPageTitle();
		$this->setMetaDescription();
		$this->setMetaKeywords();
		$this->setPageH1();
		$this->setPageH2();
		$this->setCss();
		$this->setLightBox();
	}
	
	public function setMetaContent()
	{
		$this->metaContent = array(
			"index" => array(
				"page_title"       => "Painting &amp; Decorating Contractors",
				"meta_description" => "Domestic, commercial and industrial painting and decorating contractors based in Lincolnshire with over 30 years experience.",
				"h1"               => "Painting &amp; Decorating Contractors",
				"css"              => "index.less"
			),
			"services" => array(
				"page_title"       => "Our Services",
				"meta_description" => "We provide professional painting &amp; decorating services. With over 30 years experience we are able to offer a professional solution to your project.",
				"h1"               => "Our Services",
			),
			"gallery" => array(
				"page_title"       => "Our Work",
				"meta_description" => "Take a look at some of the various painting and decorating projects we have recently undetaken.",
				"h1"               => "Our Work",
				"css"			   => "lightbox-0.5.less",
				"lightbox"		   => "yes"
			),
			"error" => array(
				"page_title"       => "Page Not Found",
				"meta_description" => "The page you have requested has not been found.",
				"h1"               => "Page Not Found",
			),
			"site_map" => array(
				"page_title"       => "Site Map",
				"meta_description" => "Links to all pages on ".config::SITE_NAME,
				"h1"               => "Site Map",
			),
			"help_about_us" => array(
				"page_title"       => "About Us",
				"meta_description" => "With over 30 years experience we are able to offer a professional painting and decorating service through Lincolnshire and the surrounding area.",
				"h1"               => "About Us",
			),
			"help_contact_us" => array(
				"page_title"       => "Contact Us",
				"meta_description" => "Contact us to get a free quote on your next painting and decorating project.",
				"h1"               => "Contact Us",
				"css"              => "forms.less"
			),
			"help_privacy" => array(
				"page_title"       => "Privacy Policy",
				"meta_description" => "Find out more about our privacy policy.",
				"h1"               => "Privacy Policy",
			)
		);
	}
	
	private function getCurrentPage()
	{
		$to_replace = array("/", "test_", ".php", "sm_", "~vjrnrejl_", "-", "__");
		$replace_with = array("_","","","","","_","_");
		
		$this->setCurrentPage(substr(str_replace($to_replace, $replace_with, ($_SERVER['PHP_SELF'])),1));
	}
	
	private function setCurrentPage($curPageRoute)
	{
		$this->currentPageRoute = $curPageRoute;
	}
	
	private function setPageTitle()
	{
		$this->pageTitle = (isset($this->metaContent[$this->currentPageRoute]["page_title"]) && $this->metaContent[$this->currentPageRoute]["page_title"] != ""
					     ? $this->metaContent[$this->currentPageRoute]["page_title"].config::PAGE_TITLE_SEPARATOR.config::SITE_NAME
					     : config::SITE_NAME);
	}
	
	private function setMetaDescription()
	{
		$this->metaDescription = (isset($this->metaContent[$this->currentPageRoute]["meta_description"]) && $this->metaContent[$this->currentPageRoute]["meta_description"] != ""
		                       ? $this->metaContent[$this->currentPageRoute]["meta_description"] 
							   : self::DEFAULT_META_DESCRIPTION);
	}
	
	private function setMetaKeywords()
	{
		$this->metaKeywords = (isset($this->metaContent[$this->currentPageRoute]["meta_keywords"]) && $this->metaContent[$this->currentPageRoute]["meta_keywords"] != ""
		                    ? $this->metaContent[$this->currentPageRoute]["meta_keywords"] 
							: self::DEFAULT_META_KEYWORDS);
	}
	
	private function setPageH1()
	{
		if(isset($this->metaContent[$this->currentPageRoute]["h1"])) {
			$this->pageH1 = $this->metaContent[$this->currentPageRoute]["h1"];
		}
	}
	
	private function setPageH2()
	{
		if(isset($this->metaContent[$this->currentPageRoute]["h2"])) {
			$this->pageH2 = $this->metaContent[$this->currentPageRoute]["h2"];
		}
	}
	
	private function setCss()
	{
		if(isset($this->metaContent[$this->currentPageRoute]["css"])) {
			$this->buildCss($this->metaContent[$this->currentPageRoute]["css"]);
		}
	}
	
	private function createCssLink($css)
	{
		return sprintf(self::STYLESHEET_LINK, config::$baseUrl, $css);
	}
	
	private function buildCss($css)
	{
		if(is_array($css)) {
			$this->css = "";
			foreach ($css as $stylesheet) {
				$this->css .= $this->createCssLink($stylesheet);
				$this->css .= "\n";
			}
		}
		else {
			$this->css = $this->createCssLink($css);
			$this->css .= "\n";
		}
	}
	
	private function setLightBox()
	{
		if(isset($this->metaContent[$this->currentPageRoute]["lightbox"]) && $this->metaContent[$this->currentPageRoute]["lightbox"] = 'yes') {
			$this->lightBox = TRUE;
		}
	}
}
?>