<?php
require_once 'resources/inc.config.php';
require_once 'resources/templates/tpl.header.php';
?>
<p class="error">Sorry but the page you are looking for has either been moved or is no longer available, take a look at the <a href="<?php echo config::$baseUrl; ?>/site-map.php" title="Site Map">site map</a> to find a list of available pages.</p>	
<?php
require_once 'resources/templates/tpl.footer.php';
?>