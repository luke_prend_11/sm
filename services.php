<?php
require_once 'resources/inc.config.php';
require_once 'resources/templates/tpl.header.php';
?>
		
		<p>We are a painting and decorating contractor offering a wide range of domestic, commercial, idustrial and education painting and decorating services. We serve Lincolnshire and the surrounding area and provide expert services including:</p>
		<ul class="list-points">
			<li>Factory and Warehouse Painting</li>
			<li>Floor Coating</li>
			<li>Cladding Spraying</li>
			<li>Steel Painting</li>
			<li>Machinery Painting</li>
			<li>Epoxy Wall And Floor Coatings</li>
			<li>Heat Resistant Coatings</li>
			<li>Non Slip Surfaces</li>
			<li>Stone and Brick Cleaning</li>
			<li>Healthcare and Hospitals</li>
		</ul>
		<p>For examples of recently completed projects, please visit our <a href="<?php echo config::$baseUrl; ?>/gallery.php" title="Recent Projects">gallery</a> page.</p>
		<p>We are not limited to the above so if you require any work not listed then please <a href="<?php echo config::$baseUrl; ?>/help/contact-us.php" title="Contact Us">contact us</a> and we will be happy to help.</p>
		
<?php
require_once 'resources/templates/tpl.footer.php';
?>