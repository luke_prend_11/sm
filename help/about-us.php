<?php
require_once '../resources/inc.config.php';
require_once '../resources/templates/tpl.header.php';
?>

		<p>Sean Moloney Ltd is a professional painting and decorating contractor based in Scunthorpe, North Lincolnshire offering quality services for domestic, commercial, industrial and education projects.</p>
		<p>With over 30 years’ experience you can rest assured that your project will be completed to the highest standard, we are committed to providing the best possible service with the utmost dedication and professionalism.</p>
		<p>We are trustworthy, flexible and reliable and guarantee to complete your project on budget with beautiful results with minimal disruption.</p>
		<p>Our extensive knowledge of protective coatings and paints, intense safety program, time management and superior quality of work is what every customer can expect.</p>
		<p>We have been providing an extensive range of specialist services since 1986. Whether you require industrial machinery painting or a school re-decorating our fully trained and expert staff will be happy to complete your project on time and within budget.</p>
		
		<a href="<?php echo config::$baseUrl; ?>/help/contact-us.php" title="Contact Us" class="btn dark-grey float-left">
			Contact us today for A FREE quote on your next project
		</a>
		
<?php
require_once '../resources/templates/tpl.footer.php';
?>