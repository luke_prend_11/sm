<?php
require_once '../resources/inc.config.php';
require_once '../resources/templates/tpl.header.php';
?>
	<div class="help-item">
		<h2>About Us</h2>
		<p>With over 30 years experience <?php echo config::SITE_NAME; ?> offers a professional painting and decorating service in the Lincolnshire area.</p>
		<p><a href="<?php echo config::$baseUrl; ?>/help/about-us.php" title="About Us">About Us</a></p>
	</div>
	<div class="help-item">
		<h2>Contact Us</h2>
		<p>For a free estimate on your next painting and decorating project or for any other enquiries please contact us and we will get back to you asap.</p>
		<p><a href="<?php echo config::$baseUrl; ?>/help/contact-us.php" title="Contact Us">Contact Us</a></p>
	</div>
	<div class="help-item-last">
		<h2>Privacy Policy</h2>
		<p>View our websites Privacy Policy.</p>
		<p><a href="<?php echo config::$baseUrl; ?>/help/privacy-policy.php" title="Privacy Policy">Privacy Policy</a></p>
	</div>
<?php
require_once '../resources/templates/tpl.footer.php';
?>