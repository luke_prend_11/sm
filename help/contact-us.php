<?php
require_once '../resources/inc.config.php';
require_once '../resources/templates/tpl.header.php';
?>

<?php
$uf = new user_functions();
if (isset($_POST['submitform'])) {
	$errors = $uf->checkContactForm($_POST['name'], $_POST['email_address'], $_POST['comments']);
	if(empty($errors)) {
		if ($uf->sendContactForm($_POST['name'], $_POST['email_address'], config::CONTACT_EMAIL, $_POST['comments'])) {
			// display error if contact form fails to send
			notifications::showSuccess('Thank you for contacting '.config::SITE_NAME.'.<br />We aim to respond to your message within 24 hours.');
		}
		else {
			// display error if contact form fails to send
			notifications::showError('Sorry but there appears to have been an error, please try again.');
			// display contact form
			$uf->contactForm();
		}
	}
	else {
		// display any errors that have occured
		notifications::showError($errors);
		// display contact form
		$uf->contactForm();
	}
}
else {
	//$uf->contactForm();
}
?>
<div id="contact-info" class="very-light-grey">
	<h2>Contact Information</h2>
	<span class="address"><?php echo config::CONTACT_ADDRESS; ?></span>
	<span class="phone">
		<a href="tel:<?php echo config::CONTACT_PHONE; ?>" title="Call <?php echo config::SITE_NAME; ?>">
			<?php echo config::CONTACT_PHONE; ?>
		</a>
	</span>
	<span class="phone">
		<a href="tel:<?php echo config::CONTACT_PHONE_ALT; ?>" title="Call <?php echo config::SITE_NAME; ?>">
			<?php echo config::CONTACT_PHONE_ALT; ?>
		</a>
	</span>
	<span class="email">
		<a href="mailto:<?php echo config::CONTACT_EMAIL; ?>" title="Email <?php echo config::SITE_NAME; ?>">
			<?php echo config::CONTACT_EMAIL; ?>
		</a>
	</span>
</div>
<?php
require_once '../resources/templates/tpl.footer.php';
?>
