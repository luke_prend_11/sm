<?php
require_once 'resources/inc.config.php';
require_once 'resources/templates/tpl.header.php';
?>
		<h1 class="home">
			<a href="<?php echo config::$baseUrl; ?>/gallery.php#domestic" title="Domestic Painting &amp; Decorating Projects">Domestic</a><br>
			<a href="<?php echo config::$baseUrl; ?>/gallery.php#commercial" title="Commercial Painting &amp; Decorating Projects">Commercial</a><br>
			<a href="<?php echo config::$baseUrl; ?>/gallery.php#industrial" title="Industrial Painting &amp; Decorating Projects">Industrial</a><br>
			<a href="<?php echo config::$baseUrl; ?>/gallery.php#education" title="Education Painting &amp; Decorating Projects">Education</a><br>
			<span>Painting and Decorating Contractors</span>
		</h1>
		
		<p><?php echo config::SITE_NAME; ?> is an established Painting and Decorating Contractor, offering a range of high quality commercial, Industrial, education and domestic services for over 30 years.</p>
		<p>We have a highly motivated workforce taking on a wide variety of projects throughout Lincolnshire, Yorkshire and the surrounding area. We will manage every detail of your project from initial contact through to completion and our qualified team of specialists will ensure you receive a professional and quality service.</p>
		<p>Our employees are fully trained and we only use premium quality products meaning you are sure to get excellent results at an affordable rate.</p>
		<p>We offer a range of painting and decorating services to domestic, commercial, industrial and education including:</p>
		<ul class="list-points">
			<li>Factory and Warehouse Painting</li>
			<li>Floor Coating</li>
			<li>Cladding Spraying</li>
			<li>Steel Painting</li>
			<li>Machinery Painting</li>
			<li>Epoxy Wall And Floor Coatings</li>
			<li>Heat Resistant Coatings</li>
			<li>Non Slip Surfaces</li>
			<li>Stone and Brick Cleaning</li>
			<li>Healthcare and Hospitals</li>
		</ul>
		<a href="<?php echo config::$baseUrl; ?>/help/contact-us.php" title="Contact Us" class="btn dark-grey float-left">
			Contact us today for A FREE quote on your next project
		</a>
<?php
require_once 'resources/templates/tpl.footer.php';
?>